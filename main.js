const {  saveData } = require('./todo-app')
const readline = require('node:readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const util = require('node:util');
const question = util.promisify(rl.question).bind(rl);

const main = async () => {
  const mh = {};
  try {
    mh.Nim = await question('Masukkan NIM : ');
    mh.Nama = await question('Masukann Nama : ');
    mh.Kota = await question('Masukkan Asal Kota : ');
    mh.TanggalLahir = await question('Masukkan Tanggal Lahir : ');

    
    if(mh.Nim,mh.Nama,mh.Kota,mh.TanggalLahir.trim().length > 0) {
      saveData(mh)
    }else{
      throw new Error(`empty`);
    }
  } catch (err) {
    console.error('Question rejected', err);
  } finally {
    rl.close()  
  }
}
// 2962805 - Reggy Charles Imanuel Lastu - SW 
// 2751797 - FERRY - SW
main()
